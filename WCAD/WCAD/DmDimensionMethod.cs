﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCAD
{
    public static class DmDimensionMethod
    {
        /// <summary>
        /// 添加转角标注
        /// </summary>
        /// <param name="line1Point">起点</param>
        /// <param name="line2Point">终点</param>
        /// <param name="dis">偏移距离</param>
        /// <returns></returns>
        public static RotatedDimension AddRotatedDimension(Point3d line1Point,Point3d line2Point,double dis)
        {
            Vector3d vector = line2Point - line1Point;
            double rotation = vector.GetAngleTo(Vector3d.XAxis,-Vector3d.ZAxis);
            vector=vector.RotateBy(Math.PI / 2, Vector3d.ZAxis);
            vector = vector.GetNormal() * dis;
            Line line = new Line(line1Point, line2Point);
            
            Point3d dimensionLinePoint = line.GetPointAtDist(line.Length/2)+ vector;
            Document doc = Application.DocumentManager.MdiActiveDocument;
            ObjectId dimensionStyle = doc.Database.DimStyleTableId;
            RotatedDimension rotatedDimension = new RotatedDimension(rotation, line1Point, line2Point, dimensionLinePoint, "", dimensionStyle);
            return rotatedDimension;
        }
    }
}
