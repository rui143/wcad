﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows.Data;

namespace WCAD.Kean
{
    public class DataStuff
    {
        [CommandMethod("HPS")]

        static public void GetHatchPatterns()
        {
            Editor ed =Application.DocumentManager.MdiActiveDocument.Editor;
            foreach (var str in HatchPatterns.Instance.AllPatterns)
                ed.WriteMessage("\n" + str.ToString());
        }
        [CommandMethod("DUMPBOL")]
        static public void DumpBindableObjects()
        {
            Editor ed =Application.DocumentManager.MdiActiveDocument.Editor;
            // Get our set of collections
            var cols = Application.UIBindings.Collections;
            // Use reflection to determine the properties of type
            // DataItemCollection (could extend to support others, too)
            var t = cols.GetType();
            var tprops = t.GetProperties();
            foreach (var tprop in tprops)
            {
                // Make sure we only deal with DataItemCollection properties
                // that do not take any parameters
                if (tprop.PropertyType == typeof(DataItemCollection) &&tprop.GetGetMethod().GetParameters().Length == 0)
                {
                    // Dump our the property name first
                    ed.WriteMessage("\n{0}:", tprop.Name);
                    // Get the collection itself and iterate through it
                    var col = (DataItemCollection)tprop.GetValue(cols, null);
                    foreach (var desc in col)
                    {
                        // Get the collection's property descriptors
                        var props = desc.GetProperties();
                        try
                        {
                            // Dump the value of each "Name" property
                            ed.WriteMessage(" \"{0}\"", props["Name"].GetValue(desc));
                        }
                        catch
                        {
                            // Just in case no "Name" property exists, we try-catch
                        }
                    }
                }
            }
        }
    }
}
