﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCAD.Kean
{
    public class CreateTableWithStyleAndWhatStyle
    {
        //https://www.keanw.com/2009/01/more-fun-with-autocad-tables-and-their-styles-using-net.html
        [CommandMethod("CTWS")]
        [Obsolete]
        static public void CTWS()
        {
            Document doc =Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;
            PromptPointResult pr =ed.GetPoint("\nEnter table insertion point: ");
            if (pr.Status == PromptStatus.OK)
            {
                Transaction tr =doc.TransactionManager.StartTransaction();
                using (tr)
                {
                    // First let us create our custom style,
                    //  if it doesn't exist
                    const string styleName = "Garish Table Style";
                    ObjectId tsId = ObjectId.Null;
                    DBDictionary sd =(DBDictionary)tr.GetObject(db.TableStyleDictionaryId,OpenMode.ForRead);
                    // Use the style if it already exists
                    if (sd.Contains(styleName))
                    {
                        tsId = sd.GetAt(styleName);
                    }
                    else
                    {
                        // Otherwise we have to create it
                        TableStyle ts = new TableStyle();
                        // Make the header area red
                        ts.SetBackgroundColor(Color.FromColorIndex(ColorMethod.ByAci, 1),(int)(RowType.TitleRow |RowType.HeaderRow));
                        // And the data area yellow
                        ts.SetBackgroundColor(Color.FromColorIndex(ColorMethod.ByAci, 2),(int)RowType.DataRow);
                        // With magenta text everywhere (yeuch :-)
                        ts.SetColor(Color.FromColorIndex(ColorMethod.ByAci, 6),(int)(RowType.TitleRow |RowType.HeaderRow |RowType.DataRow));
                        // And now with cyan outer grid-lines
                        ts.SetGridColor(Color.FromColorIndex(ColorMethod.ByAci, 4),(int)GridLineType.OuterGridLines,(int)(RowType.TitleRow |RowType.HeaderRow |RowType.DataRow));
                        // And bright green inner grid-lines
                        ts.SetGridColor(Color.FromColorIndex(ColorMethod.ByAci, 3),(int)GridLineType.InnerGridLines,(int)(RowType.TitleRow |RowType.HeaderRow |RowType.DataRow));
                        // And we'll make the grid-lines nice and chunky
                        ts.SetGridLineWeight(LineWeight.LineWeight211,(int)GridLineType.AllGridLines,(int)(RowType.TitleRow |RowType.HeaderRow |RowType.DataRow));
                        // Add our table style to the dictionary
                        //  and to the transaction
                        tsId = ts.PostTableStyleToDatabase(db, styleName);
                        tr.AddNewlyCreatedDBObject(ts, true);
                    }
                    BlockTable bt =(BlockTable)tr.GetObject(doc.Database.BlockTableId,OpenMode.ForRead);
                    Table tb = new Table();
                    tb.NumRows = 6;
                    tb.NumColumns = 3;
                    tb.SetRowHeight(3);
                    tb.SetColumnWidth(15);
                    tb.Position = pr.Value;
                    // Use our table style
                    if (tsId == ObjectId.Null)
                        // This should not happen, unless the
                        //  above logic changes
                        tb.TableStyle = db.Tablestyle;
                    else
                        tb.TableStyle = tsId;
                    // Create a 2-dimensional array
                    // of our table contents
                    string[,] str = new string[6, 3];
                    str[0, 0] = "Material Properties Table";
                    str[1, 0] = "Part No.";
                    str[1, 1] = "Name";
                    str[1, 2] = "Material";
                    str[2, 0] = "1876-1";
                    str[2, 1] = "Flange";
                    str[2, 2] = "Perspex";
                    str[3, 0] = "0985-4";
                    str[3, 1] = "Bolt";
                    str[3, 2] = "Steel";
                    str[4, 0] = "3476-K";
                    str[4, 1] = "Tile";
                    str[4, 2] = "Ceramic";
                    str[5, 0] = "8734-3";
                    str[5, 1] = "Kean";
                    str[5, 2] = "Mostly water";
                    // Use a nested loop to add and format each cell
                    for (int i = 0; i < 6; i++)
                    {
                        if (i == 0)
                        {
                            // This is for the title
                            tb.SetTextHeight(0, 0, 1);
                            tb.SetTextString(0, 0, str[0, 0]);
                            tb.SetAlignment(0, 0, CellAlignment.MiddleCenter);
                        }
                        else
                        {
                            // These are the header and data rows
                            for (int j = 0; j < 3; j++)
                            {
                                tb.SetTextHeight(i, j, 1);
                                tb.SetTextString(i, j, str[i, j]);
                                tb.SetAlignment(i, j, CellAlignment.MiddleCenter);
                            }
                        }
                    }
                    tb.GenerateLayout();
                    BlockTableRecord btr =(BlockTableRecord)tr.GetObject(bt[BlockTableRecord.ModelSpace],OpenMode.ForWrite);
                    btr.AppendEntity(tb);
                    tr.AddNewlyCreatedDBObject(tb, true);
                    tr.Commit();
                }
            }
        }
    }
}
