﻿using Autodesk.AutoCAD.Colors;
using Autodesk.AutoCAD.DatabaseServices;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;

namespace WCAD
{
    public class CADWindows
    {
        /// <summary>
        /// 颜色选择窗体
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static bool SelectColor(out Color color)
        {
            color = new Color();
            Autodesk.AutoCAD.Windows.ColorDialog colorDialog = new Autodesk.AutoCAD.Windows.ColorDialog();
            DialogResult dialogResult = colorDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                color = colorDialog.Color;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 线型选择窗体
        /// </summary>
        /// <param name="lineType"></param>
        /// <returns></returns>
        public static bool SelectLinetype(out ObjectId lineType)
        {
            lineType = ObjectId.Null ;
            Autodesk.AutoCAD.Windows.LinetypeDialog linetypeDialog = new Autodesk.AutoCAD.Windows.LinetypeDialog();
            DialogResult dialogResult = linetypeDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                lineType = linetypeDialog.Linetype;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 线宽选择窗体
        /// </summary>
        /// <param name="lineWeight"></param>
        /// <returns></returns>
        public static bool SelectLineWeight(out LineWeight lineWeight)
        {
            lineWeight = new LineWeight();
            Autodesk.AutoCAD.Windows.LineWeightDialog lineWeightDialog = new Autodesk.AutoCAD.Windows.LineWeightDialog();
            DialogResult dialogResult = lineWeightDialog.ShowDialog();
            if (dialogResult == DialogResult.OK)
            {
                lineWeight = lineWeightDialog.LineWeight;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 选择文件窗体
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="fileType">文件类型 中文</param>
        /// <param name="extension">后缀</param>
        /// <param name="file">文件路径</param>
        /// <returns></returns>
        public static bool OpenFileDialog(string title,string fileType,string extension,out string file)
        {
            file = "";
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = true;//该值确定是否可以选择多个文件
            dialog.Title = title;
            dialog.Filter = fileType+"(*." + extension + ")|*."+extension;
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                file=dialog.FileName;
                return true;
            }
            return false;
        }
        /// <summary>
        /// 选择文件夹窗体
        /// </summary>
        /// <param name="title">标题</param>
        /// <param name="path">文件夹路径</param>
        /// <returns></returns>
        public static bool FolderDialog(string title, out string path)
        {
            path = "";
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            folderDialog.Description = title;
            DialogResult result = folderDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                path = folderDialog.SelectedPath;
                return true;
            }
            return false;
        }
    }
}
