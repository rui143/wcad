﻿using Autodesk.AutoCAD.Colors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace WCAD
{
    public static class RgbColorMethod
    {
        public static Color Rainbow(this double a, double min, double max)
        {
            if (min > max) { double v = min; min = max; max = v; }
            if (a > max) a = max; 
            if (a < min) a = min;
            double step = (a - min);
            double numOfSteps = max - min;
            double r = 0.0;
            double g = 0.0;
            double b = 0.0;
            double h = step / numOfSteps;
            double i = (int)(h * 5);
            double f = h * 5.0 - i;
            double q = 1 - f;
            switch (i % 5)
            {
                case 0: r = 0; g = f; b = 1; break;
                case 1: r = 0; g = 1; b = q; break;
                case 2: r = f; g = 1; b = 0; break;
                case 3: r = 1; g = q; b = 0; break;
                case 4: r = 1; g = 0; b = f; break;
            }
            return Color.FromRgb((byte)(r * 255), (byte)(g * 255), (byte)(b * 255));
        }
        public static Color RainbowAll(double num,double maxnum)
        {
            var r = 0.0;
            var g = 0.0;
            var b = 0.0;
            var h = num / maxnum;
            var i = (int)(h * 6);
            var f = h * 6.0 - i;
            var q = 1 - f;
            switch (i % 6)
            {
                case 0: r = 1; g = f; b = 0; break;
                case 1: r = q; g = 1; b = 0; break;
                case 2: r = 0; g = 1; b = f; break;
                case 3: r = 0; g = q; b = 1; break;
                case 4: r = f; g = 0; b = 1; break;
                case 5: r = 1; g = 0; b = q; break;
            }

            return Color.FromRgb((byte)(r * 255), (byte)(g * 255), (byte)(b * 255));
        }
    }
}