﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Internal;
using Autodesk.Windows;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WCAD
{
    /// <summary>
    /// Window1.xaml 的交互逻辑
    /// </summary>
    public partial class Window1 : Window
    {
        MyViewModel myViewModel = new MyViewModel();
        public Window1()
        {
            InitializeComponent();
            //BitmapImage bitmapImage = new BitmapImage();
            //bitmapImage.BeginInit();
            //bitmapImage.UriSource = new Uri(@"C:\Users\Administrator\Pictures\aaa.jpg", UriKind.Relative);
            //bitmapImage.EndInit();

            //// 将 BitmapImage 对象设置为 Image 控件的源
            //gifImage.Source = bitmapImage;
            //myViewModel = new MyViewModel();
            //myViewModel.ImagePaths = new ObservableCollection<string>();
            //for (int i = 1; i < 1000; i++)
            //{
            //    string str = @"D:\WCAD本地\WCAD\WCAD\bin\Debug\DWG\" + i.ToString() + ".png";
            //    if (File.Exists(str)) myViewModel.ImagePaths.Add(str);
            //    else break;
            //}
            ////myViewModel.ImagePaths = new List<string>() { @"D:\WCAD本地\WCAD\WCAD\bin\Debug\DWG\1.png", @"D:\WCAD本地\WCAD\WCAD\bin\Debug\DWG\1.png" };
            //DataContext = myViewModel;
        }
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            string str = ((Button)sender).Tag.ToString();
            str = str.Replace(".png", ".dwg");

            if (File.Exists(str))
            {
                CADCommand cADCommand = new CADCommand();
                cADCommand.QR(str);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WCAD.FocusDoc();
            using (WCAD.DocLock)
            {
                // var ss = listBox.ItemsSource;
                CADCommand cADCommand = new CADCommand();
                string s = cADCommand.RRRR();
                if (!string.IsNullOrEmpty(s))
                {
                    myViewModel.ImagePaths.Add(@"D:\WCAD本地\WCAD\WCAD\bin\Debug\DWG\" + s + ".png");
                }
                //ss= listBox.ItemsSource;
                //listBox.Items.Refresh();
            }
        }
        private void MySelectionChangedEventHandler(object sender, SelectionChangedEventArgs e)
        {

            int selectedIndex = ((ListBox)sender).SelectedIndex;
            string str = myViewModel.ImagePaths[selectedIndex];
            str = str.Replace(".png", ".dwg");

            if (File.Exists(str))
            {
                CADCommand cADCommand = new CADCommand();
                cADCommand.QR(str);
            }


        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CADCommand cADCommand = new CADCommand();
            if (int.TryParse((sender as TextBox).Text, out int dis))
            {
                if (dis > 0)
                {
                    using (var locks = WCAD.DocLock)
                    {


                       

                    }

                }
            }
        }
    }
    public class MyViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<string> _imagePaths;
        public ObservableCollection<string> ImagePaths
        {
            get { return _imagePaths; }
            set
            {
                if (_imagePaths != value)
                {
                    _imagePaths = value;
                    OnPropertyChanged("ImagePaths");
                }
            }
        }

        public MyViewModel()
        {
            ImagePaths = new ObservableCollection<string>();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

}
