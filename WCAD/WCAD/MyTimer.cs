﻿using Autodesk.AutoCAD.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WCAD
{
    public class MyTimer
    {
        Timer _timer;
        Action<Timer, bool> _action;
        int _num;
        public MyTimer(int interval, int num, Action<Timer,bool> action)
        {
            _timer = new Timer();
            _timer.Interval = interval;
            _timer.Tick += Timer_Tick;
            _action = action;
            _timer.Start();
            _num = num;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            _num--;
            using (WCAD.DocLock)
            {
                _action.Invoke(_timer,_num == 0);
            }
            if (_num == 0)
            {
                _timer.Stop();
            }
        }
    }
}
