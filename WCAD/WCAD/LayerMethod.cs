﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCAD
{
    public static class LayerMethod
    {
        public static bool SwitchCurrentLayer(string layerName)
        {
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor editor = doc.Editor;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                LayerTable layerTable = tr.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (layerTable.Has(layerName))
                {
                    LayerTableRecord layer = tr.GetObject(layerTable[layerName], OpenMode.ForWrite) as LayerTableRecord;
                    editor.WriteMessage($"Switching to layer: {layerName}\n");
                    db.Clayer = layer.ObjectId;
                }
                else
                {
                    return false;
                }

                tr.Commit();
            }
            return true;
        }
    }
}
