﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WCAD
{
    public static class ExcelMethod
    {
        public static Application _excelApp;
        public static Workbook _workbook;
        public static Worksheet _worksheet;
        
        public static void OpenExcel(string file,bool b=true)
        {
            _excelApp = new Microsoft.Office.Interop.Excel.Application();
            _workbook = _excelApp.Workbooks.Open(file);
            _worksheet = _workbook.Sheets["Sheet1"];
            _excelApp.Visible = b;
        }
        public static void SetCell(int row, int column, object value)
        {
            if (_worksheet == null) return;
            _worksheet.Cells[row, column] = value;
        }
        public static object GetCell(int row, int column)
        {
            if (_worksheet == null) return null;
            return _worksheet.Cells[row, column].Value;
        }
        public static void Quit()
        {
            _workbook.Save();
            _excelApp.Quit();
        }
    }
}
