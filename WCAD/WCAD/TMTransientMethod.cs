﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
using OpenCvSharp.Dnn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WCAD
{
    public class TMTransientMethod
    {
        private HashSet<Entity> _ents = new HashSet<Entity>();
        private TransientManager _tm = TransientManager.CurrentTransientManager;
        public TMTransientMethod()
        {

        }
        public TMTransientMethod(List<Entity> ents)
        {
            Add(ents);
        }
        public TMTransientMethod(Entity ent)
        {
            Add(ent);
        }
        public void Add(Entity entity)
        {
            if (_ents.Add(entity))
            {
                _tm.AddTransient(entity, TransientDrawingMode.Main, 0, new IntegerCollection());
            }
        }
        public void Add<T>(List<T> entities) where T : Entity
        {
            entities.ForEach(ent => Add(ent));
        }
        public void Remove(Entity ent)
        {
            if (_ents.Remove(ent))
            {
                _tm.EraseTransient(ent, new IntegerCollection());
            }
        }
        public void Remove(List<Entity> ents)
        {
            ents.ForEach(ent => Remove(ent));
        }
        public void Update(Entity ent)
        {
            if (_ents.Contains(ent))
            {
                _tm.UpdateTransient(ent, new IntegerCollection());
            }
            else
            {
                Add(ent);
            }
        }
        public void Update<T>(List<T> ents) where T : Entity
        {
            ents.ForEach(ent => Update(ent));
        }
        public void Clear()
        {
            foreach (var item in _ents)
            {
                _tm.EraseTransient(item, new IntegerCollection());
            }
            _ents.Clear();
        }
        public bool Contains(Entity ent)
        {
            return _ents.Contains(ent);
        }
    }
}
