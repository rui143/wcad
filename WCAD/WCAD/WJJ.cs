﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static WCAD.Jigs;

namespace WCAD
{
    public class WJJ
    {
        double _angle = 0;
        Timer _timer;
        BlockReference _wj;
        BlockReference _wj1;
        BlockReference _wj2;
        BlockReference _wj3;
        int _num;
        [CommandMethod("ZuoYou")]
        public void ZuoYou()
        {
            //if (!BmBlcokMethod.GetBlockTableRecordByName("111", out BlockTableRecord btr)) return;
            //List<BlockReference> bls = UmUserMethod.SelectAllEntities<BlockReference>();
            //_wj = bls.Find(x => x.Name == "111");
            //if (_wj == null) return;
            if (_wj == null)
            {
                if (!UmUserMethod.GetEntity(out _wj)) return;
                if (!UmUserMethod.GetEntity(out _wj1)) return;
                if (!UmUserMethod.GetEntity(out _wj2)) return;
                if (!UmUserMethod.GetEntity(out _wj3)) return;
            }
            if (!UmUserMethod.GetEntity(out BlockReference bl)) return;
            if (bl.Position.X == 2200) _num = 1;
            else if (bl.Position.X == 3200) _num = 2;
            else if (bl.Position.X == 4200) _num = 3;
            else if (bl.Position.X == 5200) _num = 4;
            BlockReference b = bl.Clone() as BlockReference;
            EmEntityMethod.EditEntity(bl, () => bl.Visible = false);
            _timer = new Timer();
            _timer.Interval = 1000;
            _timer.Tick += Timer_Tick;
            //_timer.Start();
            BlockReference bb1 = _wj.Clone() as BlockReference;
            BlockReference bb2 = _wj1.Clone() as BlockReference;
            BlockReference bb3 = _wj2.Clone() as BlockReference;
            BlockReference bb4 = _wj3.Clone() as BlockReference;
            EmEntityMethod.EraseEntities(new List<BlockReference>() { _wj, _wj1, _wj2, _wj3 });
            JigJig jig = new JigJig(bl.Position + new Vector3d(0, 700, 0), (p, e1, e2) =>
            {
                Vector3d vector1 = p - bl.Position;
                Vector3d vector2 = new Vector3d(0, 700, 0);
                double angle = vector2.GetAngleTo(vector1, Vector3d.ZAxis);
                if (angle > Math.PI) angle -= Math.PI * 2;
                _angle = angle;
                b.Rotation = angle;
                e2.Add(b);
                switch (_num)
                {
                    case 1:
                        Matrix3d matrix = Matrix3d.Displacement(new Vector3d(-_angle * 20, 0, 0));
                        bb1.TransformBy(matrix);
                        bb2.TransformBy(matrix);
                        bb3.TransformBy(matrix);
                        bb4.TransformBy(matrix);

                        break;
                    case 2:
                        Matrix3d matrix1 = Matrix3d.Rotation(_angle/10, Vector3d.ZAxis, _wj1.Position);
                        bb2.TransformBy(matrix1);
                        bb3.TransformBy(matrix1);
                        bb4.TransformBy(matrix1);
                        
                        
                        break;
                    case 3:
                        Matrix3d matrix2 = Matrix3d.Rotation(_angle / 10, Vector3d.ZAxis, _wj2.Position);
                        bb3.TransformBy(matrix2);
                        bb4.TransformBy(matrix2);
                        break;
                    case 4:
                        Matrix3d matrix3 = Matrix3d.Rotation(_angle / 10, Vector3d.ZAxis, _wj3.Position);
                        bb4.TransformBy(matrix3);
                        break;
                    default:
                        break;
                }
                e2.Add(bb1);
                e2.Add(bb2);
                e2.Add(bb3);
                e2.Add(bb4);
            });
            EmEntityMethod.EditEntity(bl, () => bl.Visible = true);
            _wj = bb1;
            _wj1 = bb2;
            _wj2 = bb3;
            _wj3 = bb4;
            EmEntityMethod.AddEntity(bb1);
            EmEntityMethod.AddEntity(bb2);
            EmEntityMethod.AddEntity(bb3);
            EmEntityMethod.AddEntity(bb4);
            //_timer.Stop();
            //WCAD.Ed.Regen();
        }
        [CommandMethod("YouZuo")]
        public void YouZuo()
        {
            _wj = null;
            _wj1 = null;
            _wj2 = null;
            _wj3 = null;
        }
        int n = 0;
        private void Timer_Tick(object sender, EventArgs e)
        {
            switch (_num)
            {
                case 1:
                    EmEntityMethod.EditEntity(_wj, () => _wj.Position += new Vector3d(-_angle * 20, 0, 0));
                    
                    break;
                case 2:
                    double b1 = (double)BmBlcokMethod.GetDynBlockValue(_wj, "角度1");
                    b1 += -_angle * 0.1;
                    BmBlcokMethod.SetDynBlockValue(_wj, "角度1", b1);
                    //WCAD.Ed.Regen();
                    break;
                case 3:
                    double b2 = (double)BmBlcokMethod.GetDynBlockValue(_wj, "角度2");
                    b2 += -_angle * 0.1;
                    BmBlcokMethod.SetDynBlockValue(_wj, "角度2", b2);
                    //WCAD.Ed.Regen();
                    break;
                case 4:
                    double b3 = (double)BmBlcokMethod.GetDynBlockValue(_wj, "角度3");
                    b3 += -_angle * 0.1;
                    BmBlcokMethod.SetDynBlockValue(_wj, "角度3", b3);
                    //WCAD.Ed.Regen();
                    break;
                default:
                    break;
            }

            WCAD.Ed.Regen();
            WCAD.Ed.UpdateScreen();
            //_wj.Draw();

            //WCAD.Ed.UpdateScreen();
        }
    }
}
