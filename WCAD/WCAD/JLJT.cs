﻿using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.DwgIndx;
using Autodesk.AutoCAD.Internal.Render.RapidRT;
using Autodesk.AutoCAD.Runtime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Autodesk.AutoCAD.Internal.WSUtils;

namespace WCAD
{
    public class JLJT
    {
        [CommandMethod("CreatePng")]
        public void CreatePng()
        {
            if (CADWindows.OpenFileDialog("选择视频", "视频", "mp4", out string videoPath))
            {
                string outputPath = Path.GetDirectoryName(videoPath).AddPath(Path.GetFileNameWithoutExtension(videoPath));
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
                ConvertVideoToFrames(videoPath, outputPath + "\\", 0.5);
                Process.Start(outputPath);
            }


        }
        [CommandMethod("JLJT")]
        public void JLJTt()
        {
            if (!CADWindows.FolderDialog("选择文件夹", out string path)) return;
            List<string> file = Directory.GetFiles(path).ToList();
            file = file.OrderBy(x => int.Parse(Path.GetFileNameWithoutExtension(x))).ToList();
            List<char[,]> bitmaps = new List<char[,]>();
            int n = 10;
            foreach (var item in file)
            {
                Bitmap image = new Bitmap(item);
                image = ResizeImage(image, image.Width / n, image.Height / n);
                int[,] pixelValues = ConvertImageToPixelValues(image);
                char[,] chars = DisplayImage(pixelValues);
                bitmaps.Add(chars);
            }
            TMTransientMethod tm = new TMTransientMethod();
            for (int i = 200; i < 250; i++)
            {
                List<DBText> texts = new List<DBText>();
                for (int y = 0; y < bitmaps[i].GetLength(1); y++)
                {
                    for (int x = 0; x < bitmaps[i].GetLength(0); x++)
                    {
                        DBText dBText = new DBText()
                        {
                            TextString = bitmaps[i][x, y].ToString(),
                            Position = new Autodesk.AutoCAD.Geometry.Point3d(x * 5, -y * 5, 0)
                        };
                        texts.Add(dBText);
                    }
                }
                tm.Add(texts);
                WCAD.Ed.UpdateScreen();
                tm.Clear();
            }
        }
        [CommandMethod("JLJT1")]
        public void JLJTt1()
        {

        }
        // 调整图像大小
        static Bitmap ResizeImage(System.Drawing.Image image, int width, int height)
        {
            Bitmap resizedImage = new Bitmap(width, height);
            using (Graphics g = Graphics.FromImage(resizedImage))
            {
                g.DrawImage(image, 0, 0, width, height);
            }
            return resizedImage;
        }

        // 将图像转换为数字表示
        static int[,] ConvertImageToPixelValues(Bitmap image)
        {
            int[,] pixelValues = new int[image.Width, image.Height];

            for (int y = 0; y < image.Height; y++)
            {
                for (int x = 0; x < image.Width; x++)
                {
                    Color pixelColor = image.GetPixel(x, y);
                    int brightness = (pixelColor.R + pixelColor.G + pixelColor.B) / 3;
                    pixelValues[x, y] = brightness;
                }
            }

            return pixelValues;
        }

        // 在控制台中显示图像
        static char[,] DisplayImage(int[,] pixelValues)
        {
            char[,] chars = new char[pixelValues.GetLength(0), pixelValues.GetLength(1)];
            for (int y = 0; y < pixelValues.GetLength(1); y++)
            {
                for (int x = 0; x < pixelValues.GetLength(0); x++)
                {
                    int brightness = pixelValues[x, y];
                    char pixelChar = GetPixelChar(brightness);
                    chars[x, y] = pixelChar;
                }
            }
            return chars;
        }

        // 根据亮度级别选择对应的数字字符
        static char GetPixelChar(int brightness)
        {
            char[] charMap = "@%#*+=-:. ".ToCharArray();
            int charIndex = (int)Math.Round(brightness * (charMap.Length - 1) / 255.0);
            return charMap[charIndex];
        }
        public void ConvertVideoToFrames(string videoPath, string outputPath, double frameInterval)
        {
            using (var process = new Process())
            {
                process.StartInfo.FileName = @"D:\WCAD本地\ffmpeg.exe";
                process.StartInfo.Arguments = $"-i {videoPath} -vf fps={1.0f / frameInterval} {outputPath}%d.jpg";
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;

                process.Start();
                process.WaitForExit();
            }
        }

    }
}
