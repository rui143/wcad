﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Internal;
using Autodesk.AutoCAD.Runtime;
using Autodesk.AutoCAD.Windows;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using WCAD.Properties;
using System.Drawing.Imaging;
using Autodesk.AutoCAD.GraphicsInterface;
using Polyline = Autodesk.AutoCAD.DatabaseServices.Polyline;
using Autodesk.AutoCAD.Geometry;
using System.IO;

namespace WCAD
{
    public static class WCAD
    {
        //当前开的文档
        public static Document Doc { get { return Application.DocumentManager.MdiActiveDocument; } }
        //当前数据库
        public static Database Db { get { return Doc.Database; } }
        //当前编辑器
        public static Editor Ed { get { return Doc.Editor; } }
        public static DateTime dt;
        public static void ShowTime()
        {
            Ed.WriteMessage((DateTime.Now- dt).ToString("fff") +"\n");
            dt = DateTime.Now;
        }
        public static void StartTime()
        {
            dt=DateTime.Now;
        }
        //开启事务
        public static Transaction Trans { get { return Db.TransactionManager.StartTransaction(); } }
        //开启文档锁
        public static DocumentLock DocLock { get { return Doc.LockDocument(); } }

        //[System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "SetFocus")]
        //private static extern int SetFocus(IntPtr hWnd);
        //聚焦到文档
        public static void FocusDoc()
        {
            Doc.Window.Focus();
            //SetFocus(Doc.Window.Handle);
        }
        /// <summary>
        /// 获取系统变量（抄ifox的）
        /// </summary>
        /// <param name="name"></param>
        public static object GetSysVar(string name)
        {
            return Application.GetSystemVariable(name);
        }
        /// <summary>
        /// 设置系统变量（抄ifox的）
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void SetSysVar(string name, object value)
        {
            Application.SetSystemVariable(name, value);
        }
        /// <summary>
        /// 记笔记用的
        /// </summary>
        public static void HHHHHH()
        {
            
            Utils.ActivateLayout(WCAD.Doc, 1);//切换模型空间


            if (!UmUserMethod.GetEntity(out Dimension dimension)) return;//改标注，参数化
            UmUserMethod.GetDouble(out double d);

            EmEntityMethod.EditEntity(dimension, () =>
            {
                dimension.DimensionText = "d1=" + d.ToString();
            });
            WCAD.Db.RetainOriginalThumbnailBitmap = true;//预览图
        }
        //public static readonly string CACHEPATH = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";
        
        #region 笔记
        //cvport  当前视口的标识号
        //CURSORTYPE鼠标样式
        #endregion
        private static ImageBGRA32 _FormatBrushImage = null;
        public static void CreateFormatBrushCursor()
        {
            var bmp = Properties.Resources.aaa;
            //bmp.MakeTransparent(Color.White);
            //for (var i = 0; i < bmp.Width; i++)
            //{
            //    for (var j = 0; j < bmp.Height; j++)
            //    {
            //        var pixel = bmp.GetPixel(i, j);
            //        if (pixel.A == 0)
            //        {
            //            bmp.SetPixel(i, j, System.Drawing.Color.Magenta);
            //        }
            //        else
            //        {
            //            bmp.SetPixel(i, j, System.Drawing.Color.White);
            //        }
            //    }
            //}

            _FormatBrushImage = Utils.ConvertBitmapToAcGiImageBGRA32Ex(bmp);
            CursorBadgeUtilities cub = new CursorBadgeUtilities();
            cub.AddSupplementalCursorImage(_FormatBrushImage, 1);
        }

        public static void DeleteFormatBrushCursor()
        {
            if (_FormatBrushImage == null)
            {
                _FormatBrushImage = Utils.ConvertBitmapToAcGiImageBGRA32(Properties.Resources.aaa);

            }
            var cbu = new CursorBadgeUtilities();
            if (cbu.HasSupplementalCursorImage())
            {
                cbu.RemoveSupplementalCursorImage(_FormatBrushImage);
                _FormatBrushImage = null;
            }

        }

        public static Polyline MakeCloud(this Point3d pta, Point3d ptb, int Num = 10, double MinSep = 200, double MaxSep = 800, double With = 20)
        {
            var tmpCurve = new Polyline();
            tmpCurve.AddVertexAt(0, new Point2d(pta.X, pta.Y), 0, 0, 0);
            tmpCurve.AddVertexAt(1, new Point2d(ptb.X, pta.Y), 0, 0, 0);
            tmpCurve.AddVertexAt(2, new Point2d(ptb.X, ptb.Y), 0, 0, 0);
            tmpCurve.AddVertexAt(3, new Point2d(pta.X, ptb.Y), 0, 0, 0);
            tmpCurve.Closed = true;
            var dLength = tmpCurve.GetDistanceAtParameter(tmpCurve.EndParam);
            var NumMin = dLength / MaxSep;
            var NumMax = dLength / MinSep;
            var Nums = ((int)Math.Max(Math.Max(NumMin, NumMax), Num));
            var Dis = dLength / Nums;
            var pl = new Polyline();
            for (int i = 0; i < Nums; i++)
            {
                var pt = tmpCurve.GetPointAtDist(i * Dis).Convert2d(new Plane());
                pl.AddVertexAt(i, pt, -0.52, 0, With * 5);
            }
            pl.Closed = true;
            return pl;
        }
    }
}
